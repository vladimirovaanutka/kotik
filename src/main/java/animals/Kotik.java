package animals;

public class Kotik {
    private static final int METHODS = 5;
    private String name;
    private String voice;
    private int satiety; // сытость
    private int weight; //вес
    private static int count;

    private int countOfSatietyUnits;

    public Kotik(String name, String voice, int satiety, int weight) {
        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        Kotik.count++;
    }

    public Kotik() {
        Kotik.count++;
    }

    public boolean play() {
        if (satiety > 0) {
            satiety--;
            return true;

        } else {
            return false;
        }
    }

    public boolean sleep() {
        if (satiety > 0) {
            satiety--;
            return true;

        } else {
            return false;
        }
    }

    public boolean wash() {
        if (satiety > 0) {
            satiety--;
            return true;

        } else {
            return false;
        }
    }

    public boolean walk() {
        if (satiety > 0) {
            satiety--;
            return true;

        } else {
            return false;
        }
    }

    public boolean hunt() {
        if (satiety > 0) {
            satiety--;
            return true;

        } else {
            return false;
        }
    }

    public void eat(int countOfSatietyUnits) {
        satiety += countOfSatietyUnits;
    }

    public void eat(int countOfSatietyUnits, String nameOfFood) {
        satiety += countOfSatietyUnits;
    }

    public void eat() {
        eat(10, "рыб");
    }

    public String[] liveAnotherDay() {
        String[] day = new String[24];
        for (int x = 0; x < day.length; x++) {
            int random = (int) (Math.random() * METHODS) + 1;
            switch (random) {
                case 1: {
                    boolean result = play();
                    if (result) {
                        day[x] = x + " - играл";
                    } else {
                        eat();
                        day[x] = x + " - ел";
                    }
                }
                break;
                case 2: {
                    boolean result = sleep();
                    if (result) {
                        day[x] = x + " - спал";
                    } else {
                        eat();
                        day[x] = x + " - ел";
                    }
                }
                break;
                case 3: {
                    boolean result = wash();
                    if (result) {
                        day[x] = x + " - умывался";
                    } else {
                        eat();
                        day[x] = x + " - ел";
                    }
                }
                break;
                case 4: {
                    boolean result = walk();
                    if (result) {
                        day[x] = x + " - гулял";
                    } else {
                        eat();
                        day[x] = x + " - ел";
                    }
                }
                break;
                case 5: {
                    boolean result = hunt();
                    if (result) {
                        day[x] = x + " - охотился";
                    } else {
                        eat();
                        day[x] = x + " - ел";
                    }
                }
                break;
            }
        }
        return day;
    }

    public String getName() {
        return name;
    }

    public String getVoice() {
        return voice;
    }

    public int getSatiety() {
        return satiety;
    }

    public double getWeight() {
        return weight;
    }

    public static int getCount() {
        return count;
    }

    public int getCountOfSatietyUnits() {
        return countOfSatietyUnits;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setCountOfSatietyUnits(int countOfSatietyUnits) {
        this.countOfSatietyUnits = countOfSatietyUnits;
    }
}
