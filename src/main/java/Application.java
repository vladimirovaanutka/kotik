import animals.Kotik;

public class Application {
    public static void main(String[] args) {

        Kotik kotik1 = new Kotik("Барсик", "Мурр", 3, 4);
        Kotik kotik2 = new Kotik();
        kotik2.setName("Вася");
        kotik2.setVoice("Мяу-мяу");
        kotik2.setSatiety(0);
        kotik2.setWeight(2);

        String[] hours = kotik2.liveAnotherDay();

        for (String hour : hours) {
            System.out.println(hour);
        }

        System.out.println("Имя: " + kotik1.getName() + ", вес: " + kotik1.getWeight() + " кг");

        compareVoice(kotik1, kotik2);

        System.out.println("Создано котиков - " + Kotik.getCount());


    }

    static boolean compareVoice(Kotik kotik1, Kotik kotik2) {
        boolean result = kotik1.getVoice().equals(kotik2.getVoice());
        if (result) {
            System.out.println("Котики " + kotik1.getName() + " и " + kotik2.getName() + " мяукают одинаково");
        } else {
            System.out.println("Котики " + kotik1.getName() + " и " + kotik2.getName() + " мяукают по-разному");
        }
        return result;
    }
}
